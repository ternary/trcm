/*  TRCMath.hpp - Ternary Research Corporation Math C++ library

    Copyright (C) 2018, TERNARY RESEARCH CORPORATION <soft@ternary.info>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __TRCMATH_HPP
#define __TRCMATH_HPP

#define VERSION "0.0.1d"

#include <map>
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stdexcept>

#define DEBUG

#ifndef DISABLE_CMACROS
#define STRING(s) #s
#define INSTANCE(x,y) class x##y : public x { public: x##y() : x(STRING(x##y)) {
#define SUBINSTANCE(x,y) class x##y : public x { public: x##y(std::string s) : x(s.c_str()) {
#define NAMED(z) }}z
#endif

namespace TRC
{

 // Wire states:
 const char TRUE     = 'P'; /** connected to positive voltage */
 const char MAYBE    = 'O'; /** connected to intermedeate voltage */
 const char FALSE    = 'N'; /** connected to the ground */
 const char ANYBIT   = 'X'; /** for comparisons only */
 const char ANYTRIT  = 'Y'; /** for comparisons only */
 const char NC       = 'Z'; /** not connected (high impendance) */
 const char PULLUP   = '1'; /** weak pull-up to positive voltage */
 const char PULLDOWN = '0'; /** weak pull-down to the ground */
 const char PULLMID  = '-'; /** weak pull-middle to intermediate voltage */
 const char INVALID  = '?'; /** conflict on the wire (simulation halts) */

 // Aliases:
 const char HIGHIMP  = NC;
 const char POSITIVE = TRUE;
 const char NEUTRAL  = MAYBE;
 const char NEGATIVE = FALSE;

 // Types:
 const int UNSPECIFIED = 0x00000000;
 const int UINT        = 0x00010000;
 const int SINT        = 0x00110000;
 const int TINT        = 0x01110000;
 const int BFIXED      = 0x00120000; // FUTURE
 const int TFIXED      = 0x01120000; // FUTURE
 const int TFLOAT      = 0x01140000; // FUTURE

 // Masks:
 const int SIZEMASK    = 0x0000FFFF;
 const int TYPEMASK    = 0x0FFF0000;
 const int RESERVED    = 0x30000000;
 const int OVERFLOW    = 0xC0000000;

// class RuntimeException

 class RuntimeException : public std::runtime_error
 {
  public:
    RuntimeException(const char *s) : std::runtime_error(s) { }
 };

// class Wires (base internal class for all wires)

 class Wires
 {

   protected:

     int t;
     char *s;

     Wires(unsigned short n, int b = UNSPECIFIED, char c = NC)
     {
        t = b + n;
        s = new char[n+1];
        for(int i=0;i<n;i++) s[i]=c;
        s[n] = 0;
     }

     Wires min(const Wires &w) const;
     Wires max(const Wires &w) const;
     Wires inv() const;

   public:

     virtual ~Wires()
     {
        delete s;
     }

     const char* str() const
     {
        return s;
     }

     int size() const
     {
        return t & SIZEMASK;
     }

     int type() const
     {
        return t & TYPEMASK;
     }

     int overflow() const
     {
        int o = t & OVERFLOW;
        if(o<0) return -1;
        if(o>0) return 1;
        return 0;
     }

     int all() const
     {
        return t;
     }

     char get(int i) const
     {
#ifdef DEBUG
        if(i<0 || i>=size()) return 0;
#endif
        return s[i];
     }

     void set(int i, char c)
     {
#ifdef DEBUG
        if(c && i>=0 && i<size())
#endif
        s[i] = c;
     }

     std::string reverse() const
     {
        std::string ss;
        for(int i=size()-1;i>=0;i--)
        {
           ss += s[i];
        }
        return ss;
     }

     std::string binarize() const
     {
        char c;
        std::string ss;
        for(int i=size()-1;i>=0;i--)
        {
           c = s[i];
           switch(s[i])
           {
             case TRUE: c='1'; break;
             case FALSE: c='0'; break;
             case MAYBE: c='?'; break;
           }
           ss += c;
        }
        return ss;
     }

     std::string ternarize() const
     {
        char c;
        std::string ss;
        for(int i=size()-1;i>=0;i--)
        {
           c = s[i];
           switch(s[i])
           {
             case PULLUP: c='P'; break;
             case PULLDOWN: c='N'; break;
             case PULLMID: c='O'; break;
           }
           ss += c;
        }
        return ss;
     }

     Wires part(int from, int to)
     {
        int n = size();
        if(from<0 || from>n || to<0 || to>n || to<from) throw RuntimeException("Wires.part() - out of range");;
        int sz = to+1-from;
        Wires w(sz,type());
        for(int i=0;i<sz;i++) w[i]=s[from+i];
        return w;
     }

     char & operator[](int i)
     {
        if(i<0 || i>=size()) throw RuntimeException("Wires[] - out of range");;
        return s[i];
     }

     const char & operator[](int i) const
     {
        if(i<0 || i>=size()) throw RuntimeException("Wires[] - out of range");;
        return s[i];
     }

     Wires operator~() const { return inv(); }

     Wires & operator=(const Wires &w)
     {
        if(this != &w)
        {
          t = w.all();
          int n = size();
          for(int i=0;i<n;i++) s[i]=w.get(i);
        }
        return *this;
     }

     bool operator==(const Wires &w) const;

     bool operator!=(const Wires &w) const { return !(*this==w); }
     bool operator==(char c) const { Wires w(size(),type(),c); return (*this==w); }
     bool operator!=(char c) const { Wires w(size(),type(),c); return (*this!=w); }

     operator bool() const { return !(*this==FALSE); }

 };

 inline std::ostream & operator<<(std::ostream &o, const Wires &w)
 {
     o << w.reverse();
     return o;
 }

// class Wire<N> (N wires)

 template<unsigned short N> class Wire : public Wires
 {
   public:

     Wire(int b = UNSPECIFIED) : Wires(N,b)
     {
     }

     Wire(const Wires &w) : Wires(N)
     {
        if(w.size()!=N) throw RuntimeException("Wire(Wires) - different sizes");
        for(int i=0;i<N;i++) s[i] = w.get(i);
     }

     Wire(const char *a) : Wires(N)
     {
        int i = 0;
        while(a[i])
        {
          s[N-1-i] = a[i];
          if(++i==N+1) break;
        }
        if(i!=N) throw RuntimeException("Wire(const char*) - different sizes");
     }

     Wire(const char c) : Wires(N)
     {
        for(int i=0;i<N;i++) s[i] = c;
     }

     Wire<N> & operator=(const Wires &w)
     {
        if(this != &w)
        {
          if(w.size()!=N) throw RuntimeException("Wire<N>=Wires - different sizes");
          for(int i=0;i<N;i++) s[i] = w.get(i);
        }
        return *this;
     }

     Wire<N> & operator=(const char c)
     {
        for(int i=0;i<N;i++) s[i] = c;
        return *this;
     }

     Wire<N> operator&(const Wires &w) const { return min(w); }
     Wire<N> operator|(const Wires &w) const { return max(w); }
 };

// class Uint<N> (unsigned N-bit integer)

 template<unsigned short N> class Uint : public Wire<N>
 {
   public:

     Uint() : Wire<N>(UINT)
     {
     }

     Uint(const Wires &w) : Wire<N>(UINT)
     {
        if(w.size()!=N) throw RuntimeException("Uint(Wires) - different sizes");
        for(int i=0;i<N;i++) this->set(i,w.get(i));
     }

     Uint(unsigned int ui) : Wire<N>(UINT)
     {
        unsigned int m = 1;
        for(int i=0;i<N;i++)
        {
          this->set(i,(ui&m)?TRUE:FALSE);
          if(m==0x80000000U) m = 0;
          else m <<= 1;
        }
     }

     Uint<N> & operator=(const Wires &w)
     {
        if(this != &w)
        {
          if(w.size()!=N) throw RuntimeException("Uint<N>=Wires - different sizes");
          for(int i=0;i<N;i++) this->set(i,w.get(i));
        }
        return *this;
     }

     Uint<N> & operator=(unsigned int ui)
     {
        Uint<N> w(ui);
        (*this) = w;
        return *this;
     }

     operator unsigned() const
     {
        std::string ss = this->binarize();
        if(ss.find(INVALID)!=std::string::npos) throw RuntimeException("(unsigned)Uint<N> - unparsable number");
        unsigned int i=0,m=1;
        for(auto it=ss.rbegin();it!=ss.rend();it++)
        {
           if(*it=='1') i |= m;
           m <<= 1;
        }
        return i;
     }

     bool operator==(const Uint<N> w)
     {
        unsigned a = (unsigned)(*this);
        unsigned b = (unsigned)w;
        return (a==b);
     }

     bool operator==(unsigned b)
     {
        unsigned a = (unsigned)(*this);
        return (a==b);
     }

     bool operator>(const Uint<N> w)
     {
        unsigned a = (unsigned)(*this);
        unsigned b = (unsigned)w;
        return (a>b);
     }

     bool operator>(unsigned b)
     {
        unsigned a = (unsigned)(*this);
        return (a>b);
     }

     bool operator<(const Uint<N> w)
     {
        unsigned a = (unsigned)(*this);
        unsigned b = (unsigned)w;
        return (a<b);
     }

     bool operator<(unsigned b)
     {
        unsigned a = (unsigned)(*this);
        return (a<b);
     }

     bool operator!=(const Uint<N> w) { return !((*this)==w); }
     bool operator!=(unsigned b)      { return !((*this)==b); }
     bool operator<=(const Uint<N> w) { return !((*this)>w); }
     bool operator<=(unsigned b)      { return !((*this)>b); }
     bool operator>=(const Uint<N> w) { return !((*this)<w); }
     bool operator>=(unsigned b)      { return !((*this)<b); }

     
 };

 template<unsigned short N> std::ostream & operator<<(std::ostream &o, const Uint<N> &w)
 {
     o << (unsigned)w;
     return o;
 }

// class Sint<N> (signed N-bit integer)

 template<unsigned short N> class Sint : public Wire<N>
 {
   public:

     Sint() : Wire<N>(SINT)
     {
     }

     Sint(const Wires &w) : Wire<N>(SINT)
     {
        if(w.size()!=N) throw RuntimeException("Sint(Wires) - different sizes");
        for(int i=0;i<N;i++) this->set(i,w.get(i));
     }

     Sint(signed int si) : Wire<N>(UINT)
     {
        unsigned int m = 1;
        for(int i=0;i<N;i++)
        {
          if(m==0) this->set(i,(si<0)?TRUE:FALSE);
          else this->set(i,(si&m)?TRUE:FALSE);
          if(m==0x80000000U) m = 0;
          else m <<= 1;
        }
     }

     Sint<N> & operator=(const Wires &w)
     {
        if(this != &w)
        {
          if(w.size()!=N) throw RuntimeException("Sint<N>=Wires - different sizes");
          for(int i=0;i<N;i++) this->set(i,w.get(i));
        }
        return *this;
     }

     Sint<N> & operator=(signed int si)
     {
        Sint<N> w(si);
        (*this) = w;
        return *this;
     }

     operator int() const
     {
        std::string ss = this->binarize();
        if(ss.find(INVALID)!=std::string::npos) throw RuntimeException("(unsigned)Uint<N> - unparsable number");
        unsigned int i=0,m=1,l=0;
        for(auto it=ss.rbegin();it!=ss.rend();it++)
        {
           if(*it=='1')
           {
              i |= m;
              l = 1;
           }
           else l = 0;
           m <<= 1;
        }
        if(l && ss.length()<32)
        {
           for(m=ss.length();m<32;m++) i|=1<<m;
        }
        return (int)i;
     }

     bool operator==(const Sint<N> w)
     {
        int a = (int)(*this);
        int b = (int)w;
        return (a==b);
     }

     bool operator==(int b)
     {
        int a = (int)(*this);
        return (a==b);
     }

     bool operator>(const Sint<N> w)
     {
        int a = (int)(*this);
        int b = (int)w;
        return (a>b);
     }

     bool operator>(int b)
     {
        int a = (int)(*this);
        return (a>b);
     }

     bool operator<(const Sint<N> w)
     {
        int a = (int)(*this);
        int b = (int)w;
        return (a<b);
     }

     bool operator<(int b)
     {
        int a = (int)(*this);
        return (a<b);
     }

     bool operator!=(const Sint<N> w) { return !((*this)==w); }
     bool operator!=(int b)           { return !((*this)==b); }
     bool operator<=(const Sint<N> w) { return !((*this)>w); }
     bool operator<=(int b)           { return !((*this)>b); }
     bool operator>=(const Sint<N> w) { return !((*this)<w); }
     bool operator>=(int b)           { return !((*this)<b); }
 };

 template<unsigned short N> std::ostream & operator<<(std::ostream &o, const Sint<N> &w)
 {
     o << (int)w;
     return o;
 }

// class Tint<N> (balanced ternary N-trit integer)

 template<unsigned short N> class Tint : public Wire<N>
 {
   public:

     Tint() : Wire<N>(TINT)
     {
     }

     Tint(const Wires &w) : Wire<N>(TINT)
     {
        if(w.size()!=N) throw RuntimeException("Tint(Wires) - different sizes");
        for(int i=0;i<N;i++) this->set(i,w.get(i));
     }

     Tint<N> & operator=(const Wires &w)
     {
        if(this != &w)
        {
          if(w.size()!=N) throw RuntimeException("Tint=Wires - different sizes");
          for(int i=0;i<N;i++) this->set(i,w.get(i));
        }
        return *this;
     }
 };

 template<unsigned short N> std::ostream & operator<<(std::ostream &o, const Tint<N> &w)
 {
     o << w.ternarize();
     return o;
 }

// class Signal (the same thing as Wire<1>)

 typedef Wire<1> Signal;

 inline Signal operator&(const Signal &a, const Signal &b)
 {
  Signal f = FALSE;
  if((bool)a) return b;
  return f;
 }

 inline Signal operator&(bool a, const Signal &b)
 {
  Signal f = FALSE;
  if(a) return b;
  return f;
 }

 inline Signal operator&(const Signal &a, bool b)
 {
  Signal f = FALSE;
  if(b) return a;
  return f;
 }

 inline Signal operator|(const Signal &a, const Signal &b)
 {
  Signal f = TRUE;
  if((bool)a) return f;
  else b;
 }

 inline Signal operator|(bool a, const Signal &b)
 {
  Signal f = TRUE;
  if(a) return f;
  return b;
 }

 inline Signal operator|(const Signal &a, bool b)
 {
  Signal f = TRUE;
  if(b) return f;
  return a;
 }

// Simulation classes:

 class Entity;

// class Connection

 class Connection
 {
     std::list<Entity*> u;
     std::string *n;
     char c[4];

     friend class System;

     void entity(Entity* ent, char init)
     {
        auto a = std::find(u.begin(),u.end(),ent);
        if(a!=u.end()) u.push_back(ent);
        apply(init);
        c[0] = c[1];
        c[2] = c[0];
        c[3] = c[0];
     }

   public:

     Connection(const char *s)
     {
        n = new std::string(s);
        c[0] = c[1] = c[2] = c[3] = NC;
     }

     ~Connection()
     {
        delete n;
     }

     const std::string & name()
     {
        return *n;
     }

     void prepare()
     {
        c[3] = c[2];
        c[2] = c[1];
        c[1] = c[0];
     }

     bool transition(char before, char now)
     {
        if(c[2]==now && c[3]==before) return true;
        return false;
     }

     int read_past()
     {
        return c[3];
     }

     int read()
     {
        return c[2];
     }

     int read_current()
     {
        return c[1];
     }

     int read_default()
     {
        return c[0];
     }

     int apply(char a);

     int operator<<(char a)
     {
        return apply(a);
     }

     int operator<<(Signal & s)
     {
        return apply(s[0]);
     }

 };

// class System (singleton)

 class System
 {
     static std::vector<Connection*> v;
     static std::map<std::string,int> m;
     static System* s;

     System()
     {
     }

   public:

     ~System()
     {
        s = nullptr;
     }

     static System* getInstance();

     int attach(Entity* e, const char* n, unsigned short k = 1, char c = NC);

     int prepare()
     {
         int n = v.size();
         for(int i=0;i<n;i++) v[i]->prepare();
         return n;
     }

     Connection* getConnection(int i)
     {
         return v[i];
     }
 };

// class Entity

 class Entity
 {
   protected:

     System *sys;

   public:

     std::string *n;

     Entity(const char *s)
     {
         sys = System::getInstance();
         n = new std::string(s);
     }

     virtual ~Entity()
     {
         delete n;
     }

     const std::string & name()
     {
         return *n;
     }

     const std::string name(const char* s)
     {
         std::string n2(*n);
         n2 += ".";
         n2 += s;
         return n2;
     }

     int at(const char* n, unsigned short k = 1, char c = NC)
     {
         return sys->attach(this,n,k,c);
     }

     Connection & io(int i)
     {
         return *(sys->getConnection(i));
     }

     bool posedge(int i)
     {
         return io(i).transition(FALSE,TRUE);
     }

     bool negedge(int i)
     {
         return io(i).transition(TRUE,FALSE);
     }

     bool halfposedge(int i)
     {
         return io(i).transition(MAYBE,TRUE);
     }

     bool halfnegedge(int i)
     {
         return io(i).transition(MAYBE,FALSE);
     }

     virtual void step() = 0;

 };


} // namespace

#endif
