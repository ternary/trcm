#include <TRCMath.hpp>
#undef DEBUG

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

using namespace TRC;

class HalfAdder : public Entity
{
 protected:

// indecies:
  int iA,iB,iS,iC;

// inputs:
  Signal A,B;

// outputs:
  Signal S,C;

 public:

  HalfAdder(const char* s) : Entity(s)
  {

// empty constructor for generic unit

  }

  void step()
  {
    A = io(iA).read();
    B = io(iB).read();

    if(A==TRUE && B==TRUE)
       S = FALSE;
    else if(A==FALSE && B==TRUE)
       S = TRUE;
    else if(A==TRUE && B==FALSE)
       S = TRUE;
    else // if(A==FALSE && B==FALSE)
       S = FALSE;

    if(A==TRUE && B==TRUE)
       C = TRUE;
    else
       C = FALSE;

//    cout << name() << ":" << A << B << "->" << C << S << endl;

    io(iS) << S;
    io(iC) << C;
  }
};

class World : public Entity
{

// indecies:
  int i_increment,i_input,i_output,i_carry;

 public:

// internal counter:
  long long counter;

  World() : Entity("World")
  {
    i_increment = at("INC");
    i_input     = at("I",8);
    i_output    = at("O",8);
    i_carry     = at("COUT");

    counter = 0;
  }

  void step() // World works backwards - read outputs and write inputs
  {
    Wire<9> vec; // temporary vector
    vec[8] = io(i_output+0).read();
    vec[7] = io(i_output+1).read();
    vec[6] = io(i_output+2).read();
    vec[5] = io(i_output+3).read();
    vec[4] = io(i_output+4).read();
    vec[3] = io(i_output+5).read();
    vec[2] = io(i_output+6).read();
    vec[1] = io(i_output+7).read();
    vec[0] = io(i_carry).read();
#ifdef DEBUG
    cout << "Case " << counter << " output=" << vec.binarize() << endl;
#endif

    io(i_increment) << TRUE; /* below we change inputs every 16 steps */
    io(i_input+0)   << ((counter&(1<<4))?TRUE:FALSE);
    io(i_input+1)   << ((counter&(1<<5))?TRUE:FALSE);
    io(i_input+2)   << ((counter&(1<<6))?TRUE:FALSE);
    io(i_input+3)   << ((counter&(1<<7))?TRUE:FALSE);
    io(i_input+4)   << ((counter&(1<<8))?TRUE:FALSE);
    io(i_input+5)   << ((counter&(1<<9))?TRUE:FALSE);
    io(i_input+6)   << ((counter&(1<<10))?TRUE:FALSE);
    io(i_input+7)   << ((counter&(1<<11))?TRUE:FALSE);

    counter++;
  }

};

unsigned char BYTE = 0;

int main()
{
  System *sys = System::getInstance();

  World world;

  INSTANCE(HalfAdder,0);
      iA = at("INC");
      iB = at("I[0]");
      iS = at("O[0]");
      iC = at("C0");
  NAMED(ha0);

  INSTANCE(HalfAdder,1);
      iA = at("C0");
      iB = at("I[1]");
      iS = at("O[1]");
      iC = at("C1");
  NAMED(ha1);

  INSTANCE(HalfAdder,2);
      iA = at("C1");
      iB = at("I[2]");
      iS = at("O[2]");
      iC = at("C2");
  NAMED(ha2);

  INSTANCE(HalfAdder,3);
      iA = at("C2");
      iB = at("I[3]");
      iS = at("O[3]");
      iC = at("C3");
  NAMED(ha3);

  INSTANCE(HalfAdder,4);
      iA = at("C3");
      iB = at("I[4]");
      iS = at("O[4]");
      iC = at("C4");
  NAMED(ha4);

  INSTANCE(HalfAdder,5);
      iA = at("C4");
      iB = at("I[5]");
      iS = at("O[5]");
      iC = at("C5");
  NAMED(ha5);

  INSTANCE(HalfAdder,6);
      iA = at("C5");
      iB = at("I[6]");
      iS = at("O[6]");
      iC = at("C6");
  NAMED(ha6);

  INSTANCE(HalfAdder,7);
      iA = at("C6");
      iB = at("I[7]");
      iS = at("O[7]");
      iC = at("COUT");
  NAMED(ha7);

  unsigned long t1,t2;
  int i,n = 100000000;
  t1 = clock();
  for(i=0;i<n;i++)
  {
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
    BYTE++;
  }
  t2 = clock();

  printf("BYTE=0x%2.2X (%6.6fs or %2.2fns per increment)\n",BYTE,
        (double)(t2-t1)/CLOCKS_PER_SEC,
        (double)(t2-t1)/(n/1e8)/CLOCKS_PER_SEC
        /* 1e8 because we have 10 increments per iteration */
        );

  n = 0x100000;
  t1 = clock();
  while(world.counter!=n)
  {
    sys->prepare();
    ha0.step();
    ha1.step();
    ha2.step();
    ha3.step();
    ha4.step();
    ha5.step();
    ha6.step();
    ha7.step();
    world.step();
  }
  t2 = clock();

  printf("%4.4fs or %2.2fns per step\n",
         (double)(t2-t1)/CLOCKS_PER_SEC,
         (double)(t2-t1)/(n/1e9)/CLOCKS_PER_SEC
        );
}
