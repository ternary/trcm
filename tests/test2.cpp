#include <TRCMath.hpp>

using namespace std;

using namespace TRC;

class HalfAdder : public Entity
{
 protected:

// indecies:
  int iA,iB,iS,iC;

// inputs:
  Signal A,B;

// outputs:
  Signal S,C;

 public:

  HalfAdder(const char* s) : Entity(s)
  {

// empty constructor for generic unit

  }

  void step()
  {
    A = io(iA).read();
    B = io(iB).read();

    if(A==TRUE && B==TRUE)
       S = FALSE;
    else if(A==FALSE && B==TRUE)
       S = TRUE;
    else if(A==TRUE && B==FALSE)
       S = TRUE;
    else // if(A==FALSE && B==FALSE)
       S = FALSE;

    if(A==TRUE && B==TRUE)
       C = TRUE;
    else
       C = FALSE;

//    cout << name() << ":" << A << B << "->" << C << S << endl;

    io(iS) << S;
    io(iC) << C;
  }
};

class World : public Entity
{

// indecies:
  int i_increment,i_input,i_output,i_carry;

// internal counter:
  int counter;

 public:

  World() : Entity("World")
  {
    i_increment = at("INC");
    i_input     = at("I",3);
    i_output    = at("O",3);
    i_carry     = at("C2");

    counter = 0;
  }

  void step() // test cases
  {
    Wire<4> vec; // temporary vector
    vec[0] = io(i_output+0).read();
    vec[1] = io(i_output+1).read();
    vec[2] = io(i_output+2).read();
    vec[3] = io(i_carry).read();
    cout << "Case " << counter << " output=" << vec << endl;

    switch(counter++)
    {
       case 0:
       case 1:
       case 2:
       case 3:
          io(i_increment) << FALSE;
          io(i_input+0)   << TRUE;
          io(i_input+1)   << TRUE;
          io(i_input+2)   << TRUE;
          break;

       case 4:
       case 5:
       case 6:
       case 7:
          io(i_increment) << TRUE;
          io(i_input+0)   << TRUE;
          io(i_input+1)   << TRUE;
          io(i_input+2)   << TRUE;
          break;

       case 8:
       case 9:
       case 10:
       case 11:
          io(i_increment) << TRUE;
          io(i_input+0)   << FALSE;
          io(i_input+1)   << FALSE;
          io(i_input+2)   << FALSE;
          break;


    }
  }

};

int main()
{
  System *sys = System::getInstance();

  World world;

  class HalfAdder0 : public HalfAdder
  {
   public:
    HalfAdder0() : HalfAdder("HalfAdder0")
    {
      iA = at("INC");
      iB = at("I[0]");
      iS = at("O[0]");
      iC = at("C0");
    }
  } ha0;

  class HalfAdder1 : public HalfAdder
  {
   public:
    HalfAdder1() : HalfAdder("HalfAdder1")
    {
      iA = at("C0");
      iB = at("I[1]");
      iS = at("O[1]");
      iC = at("C1");
    }
  } ha1;

  class HalfAdder2 : public HalfAdder
  {
   public:
    HalfAdder2() : HalfAdder("HalfAdder2")
    {
      iA = at("C1");
      iB = at("I[2]");
      iS = at("O[2]");
      iC = at("C2");
    }
  } ha2;

  for(int i=0;i<=12;i++)
  {
    sys->prepare();
    ha0.step();
    ha1.step();
    ha2.step();
    world.step();
  }
}
