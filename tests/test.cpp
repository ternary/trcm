#include <TRCMath.hpp>

using namespace std;

using namespace TRC;

class MyUnit : public Entity
{
 protected:

  // indecies:
  int i0,i1,i2;
  // inputs:
  Wire<8> net;
  // outputs:
  Signal sig;
  // internal objects:
  class InternalUnit : public Entity
  {
    public:

      int i1;

      InternalUnit(string s) : Entity(s.c_str())
      {
         i1 = at(".local");
      }

      void step()
      {

      }

  }*u1,*u2;

 public:

  MyUnit(const char* s) : Entity(s)
  {
    i0 = at("bus",8,PULLUP);
    i1 = at(".network",8);
    i2 = at("single");
    u1 = new InternalUnit(name("u1"));
    u2 = new InternalUnit(name("u2"));
  }

  void step()
  {
    for(int i=0;i<8;i++)
      net[i] = io(i1+i).read();

    sig[0] = TRUE;

    if(posedge(i1+5))
    {

    }

    io(i0) << sig[0];
    io(i0+1) << sig[0];
    io(i2) << sig[0];
  }
};


int main()
{
  Wire<5> a("11111");
  Wire<32> b = FALSE;
  Wire<1> signal,out;
  Signal signal2;
  Sint<32> s = 0;
  s[31] = TRUE;
  b[0] = TRUE;
  Uint<32> u = b | s;
  Sint<5> small = -16;
  signal = MAYBE;
  signal2 = TRUE;
  out = signal & signal2;
  cout << "small=" << small << " (" << small.binarize() << ")" << endl;
  cout << "signal=" << signal << endl;
  cout << "signal2=" << signal2 << endl;
  cout << "out=" << out << endl;
  cout << "a=" << a << endl;
  cout << "b=" << b << endl;
  cout << "s=" << s << endl;
  cout << "u=" << u << endl;
  cout << "b.part(0,2)=" << b.part(0,2).binarize() << endl;
  if(b && s) cout << "true" << endl;
  else cout << "false" << endl;

  System *sys = System::getInstance();
  class Unit1 : public MyUnit
  {
   public:
    Unit1() : MyUnit("Unit1")
    {
      i2 = at("single2");
    }
  } u1;
  sys->prepare();
  u1.step();
}
