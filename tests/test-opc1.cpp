// OPC1 test - based on https://github.com/revaldinho/opc/tree/master/opc1

#include <TRCMath.hpp>

using namespace std;

using namespace TRC;

// module opccpu( inout[7:0] data, output[10:0] address, output rnw, input clk, input reset_b); // SEE BELOW

class OPC1 : public Entity
{
  public:

//   parameter FETCH0=0, FETCH1=1, RDMEM=2, RDMEM2=3, EXEC=4 ;
   const unsigned FETCH0=0, FETCH1=1, RDMEM=2, RDMEM2=3, EXEC=4;

//   parameter AND=5'bx0000,  LDA=5'bx0001, NOT=5'bx0010, ADD=5'bx0011;
   const Wire<5> AND, LDA, NOT, ADD; // SEE BELOW

//   parameter LDAP=5'b01001, STA=5'b11000, STAP=5'b01000;
   const Wire<5> LDAP, STA, STAP; // SEE BELOW

//   parameter JPC=5'b11001,  JPZ=5'b11010, JP=5'b11011,  JSR=5'b11100;
   const Wire<5> JPC, JPZ, JP, JSR; // SEE BELOW

//   parameter RTS=5'b11101,  LXA=5'b11110;
   const Wire<5> RTS, LXA; // SEE BELOW

//   reg [10:0] OR_q, PC_q;
   Wire<11> OR_q,OR_q_REG;
   Uint<11> PC_q,PC_q_REG;

//   reg [7:0]  ACC_q;
   Uint<8> ACC_q,ACC_q_REG;

//   reg [2:0]  FSM_q;
   Uint<3> FSM_q,FSM_q_REG;

//   reg [4:0]  IR_q;
   Wire<5> IR_q,IR_q_REG;

//   reg [2:0]  LINK_q; // bottom bit doubles up as carry flag
   Wire<3> LINK_q,LINK_q_REG;
// SHOUD WE HAVE A MACROS REG(Wire<3>,LINK_q)?

//`define CARRY LINK_q[0]
#define CARRY LINK_q[0]
// BUT FOR THE LEFT SIDE WE NEED TO USE TEMPORARY COPY!!!
#define CARRY_REG LINK_q_REG[0]

// TEMPORARY SIGNALS:
   Signal reset_b, writeback_w, rnw;
   Wire<11> address;
   Wire<8> data; // it's all Z by default

// IMPLEMENTATION OF INTERFACE (inout[7:0] data, output[10:0] address, output rnw, input clk, input reset_b);

   int i_data, i_address, i_rnw, i_clk, i_reset_b;

   OPC1() : Entity("OPC1"),
            // initialization for constant Wire<5>s:
            AND("X0000"), LDA("X0001"), NOT("X0010"), ADD("X0011"),
            LDAP("01001"), STA("11000"), STAP("01000"),
            JPC("11001"), JPZ("11010"), JP("11011"), JSR("11100"),
            RTS("11101"), LXA("11110")
   {
      // global attachements (we have the only instance of the class so it will work)
      i_data = at("DATA",8,PULLUP);
      i_address = at("ADDRESS",11);
      i_rnw = at("RNW");
      i_clk = at("CLK");
      i_reset_b = at("RESET_B");
   }

   void step()
   {

     reset_b = io(i_reset_b).read();

//   wire       writeback_w = ((FSM_q == EXEC) && (IR_q == STA || IR_q == STAP)) & reset_b ;
     writeback_w = ((FSM_q == EXEC) && (IR_q == STA || IR_q == STAP)) & reset_b ;

//   assign rnw = ~writeback_w ;
     rnw = ~writeback_w;

//   assign data = (writeback_w)?ACC_q:8'bz ;
     if(writeback_w) data = ACC_q;
     else // below required for later logic
     {
        for(int i=0;i<8;i++) data[i] = io(i_data+i).read();
     }

//   assign address = ( writeback_w || FSM_q == RDMEM || FSM_q==RDMEM2)? OR_q:PC_q;
     address = ( (bool)writeback_w || FSM_q == RDMEM || FSM_q == RDMEM2 )? OR_q:PC_q;

//   always @ (posedge clk or negedge reset_b )
     if(posedge(i_clk) || negedge(i_reset_b)) // WRITE FSM_q
     {

//     if (!reset_b)
       if (!reset_b)

//       FSM_q <= FETCH0;
         FSM_q_REG = FETCH0;

//     else
       else
       {
//       case(FSM_q)
//         FETCH0 : FSM_q <= FETCH1;
         if(FSM_q_REG == FETCH0) FSM_q = FETCH1;

//         FETCH1 : FSM_q <= (IR_q[4])?EXEC:RDMEM ;
         else if(FSM_q == FETCH1) FSM_q_REG = (IR_q[4]==TRUE)?EXEC:RDMEM;

//         RDMEM  : FSM_q <= (IR_q==LDAP)?RDMEM2:EXEC;
         else if(FSM_q == RDMEM) FSM_q_REG = (IR_q==LDAP)?RDMEM2:EXEC;

//         RDMEM2 : FSM_q <= EXEC;
         else if(FSM_q == RDMEM2) FSM_q_REG = EXEC;

//         EXEC   : FSM_q <= FETCH0;
         else if(FSM_q == EXEC) FSM_q_REG = FETCH0;

//       endcase
       }
     }

//   always @ (posedge clk)
     if(posedge(i_clk)) // WRITE IR_q, OR_q, LINK_q, ACC_q and CARRY

//     begin
     {

//        IR_q <= (FSM_q == FETCH0)? data[7:3] : IR_q;
          if(FSM_q == FETCH0)
          {
            IR_q_REG[0] = data[3];
            IR_q_REG[1] = data[4];
            IR_q_REG[2] = data[5];
            IR_q_REG[3] = data[6];
            IR_q_REG[4] = data[7];
          }

//        // OR_q[10:8] is upper part nybble for address - needs to be zeroed
//        // for both pointer READ and WRITE operations once ptr val is read
//        OR_q[10:8] <= (FSM_q == FETCH0)? data[2:0]: (FSM_q==RDMEM)?3'b0:OR_q[10:8];
          if(FSM_q == FETCH0)
          {
             OR_q_REG[8] = data[0];
             OR_q_REG[9] = data[1];
             OR_q_REG[10] = data[2];
          }
          else if(FSM_q == RDMEM)
          {
             OR_q_REG[8] = FALSE;
             OR_q_REG[9] = FALSE;
             OR_q_REG[10] = FALSE;
          }

//        OR_q[7:0] <= data; //Lowest byte of OR is dont care in FETCH0 and at end of EXEC
          for(int i=0;i<8;i++) OR_q[i] = data[i];

//        if ( FSM_q == EXEC )
          if(FSM_q == EXEC)
          {

//          casex (IR_q)
//            JSR    : {LINK_q,ACC_q} <= PC_q ;
              if(IR_q==JSR)
              {
                 LINK_q_REG = PC_q.part(8,10);
                 ACC_q_REG  = PC_q.part(0,7);
              }

//            LXA    : {LINK_q,ACC_q} <= {ACC_q[2:0], 5'b0, LINK_q};
              else if(IR_q==LXA)
              {
                 LINK_q_REG = ACC_q.part(0,2);
                 for(int i=7;i>=0;i--)
                 {
                     if(i>=3) ACC_q_REG[i] = FALSE;
                     else ACC_q_REG[i] = LINK_q[i];
                 }
              }

//            AND    : {`CARRY, ACC_q}  <= {1'b0, ACC_q & OR_q[7:0]};
              else if(IR_q==AND)
              {
                 CARRY_REG = FALSE;
                 ACC_q_REG = ACC_q & OR_q.part(0,7);
              }

//            NOT    : ACC_q <= ~OR_q[7:0];
              else if(IR_q==NOT)
              {
                 ACC_q_REG = ~OR_q.part(0,7);
              }

//            LDA    : ACC_q <= OR_q[7:0];
              else if(IR_q==LDA)
              {
                 ACC_q_REG = OR_q.part(0,7);
              }

//            LDAP   : ACC_q <= OR_q[7:0];
              else if(IR_q==LDAP)
              {
                 ACC_q_REG = OR_q.part(0,7);
              }

//            ADD    : {`CARRY,ACC_q} <= ACC_q + `CARRY + OR_q[7:0];
              else if(IR_q==ADD)
              {
                 ACC_q_REG = ACC_q + (CARRY==TRUE)?1:0 + OR_q.part(0,7);
                 if(ACC_q.overflow())
                      CARRY_REG = TRUE;
                 else CARRY_REG = FALSE;
              }

//            default: {`CARRY,ACC_q} <= {`CARRY,ACC_q};
              else
              {
                 CARRY_REG = CARRY;
                 ACC_q_REG = ACC_q;
              }

//          endcase
          }

//     end
     }

//   always @ (posedge clk or negedge reset_b )
     if(posedge(i_clk) || negedge(i_reset_b)) // WRITE PC_q
     {

//     if (!reset_b) // On reset start execution at 0x100 to leave page zero clear for variables
       if(!reset_b)

//       PC_q <= 11'h100;
         PC_q_REG = 0x100;

//     else
       else

//       if ( FSM_q == FETCH0 || FSM_q == FETCH1 )
         if(FSM_q==FETCH0 || FSM_q==FETCH1)

//         PC_q <= PC_q + 1;
           PC_q_REG = PC_q + 1;

//       else
         else
         {

//         case (IR_q)
//           JP    : PC_q <= OR_q;
             if(IR_q==JP)
             {
                PC_q_REG = OR_q;
             }

//           JPC   : PC_q <= (`CARRY)?OR_q:PC_q;
             else if(IR_q==JPC)
             {
                PC_q_REG = (CARRY==TRUE)?OR_q:PC_q;
             }

//           JPZ   : PC_q <= ~(|ACC_q)?OR_q:PC_q;
             else if(IR_q==JPZ)
             {
                PC_q_REG = ((unsigned)ACC_q==0)?OR_q:PC_q;
             }

//           JSR   : PC_q <= OR_q;
             else if(IR_q==JSR)
             {
                PC_q_REG = OR_q;
             }

//           RTS   : PC_q <= {LINK_q, ACC_q};
             else if(IR_q==RTS)
             {
                for(int i=10;i>=0;i--)
                {
                   if(i>=8) PC_q_REG[i]=LINK_q[i-8];
                   else PC_q_REG[i]=ACC_q[i];
                }
             }

//           default: PC_q <= PC_q;
             else PC_q_REG = PC_q;

//         endcase
         }

         OR_q = OR_q_REG;
         PC_q = PC_q_REG;
         ACC_q = ACC_q_REG;
         FSM_q = FSM_q_REG;
         IR_q = IR_q_REG;
         LINK_q = LINK_q_REG;

         if(writeback_w)
         {
             for(int i=0;i<8;i++) io(i_data) << data[i];
         }
         for(int i=0;i<11;i++) io(i_address) << address[i];
         io(i_rnw) << rnw;
      }

//endmodule
   }
};
