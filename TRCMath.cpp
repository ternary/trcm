/*  TRCMath.cpp - Ternary Research Corporation Math C++ library

    Copyright (C) 2018, TERNARY RESEARCH CORPORATION <soft@ternary.info>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TRCMath.hpp"

//#define DEBUG_APPLY
#define DEBUG_ATTACH

using namespace TRC;

/* Wires bodies */

bool Wires::operator==(const Wires &w) const
{
        int n = size();
        if(n != w.size()) return false;
        for(int i=0;i<n;i++)
        {
           char c1 = s[i];
           char c2 = w.get(i);
           if(c1==ANYBIT || c1==ANYTRIT || c2==ANYBIT || c2==ANYTRIT)
           {
              if(c1==ANYBIT && c2!=TRUE && c2!=FALSE && c2!=PULLUP && c2!=PULLDOWN) return false;
              if(c1==ANYTRIT && c2!=TRUE && c2!=MAYBE && c2!=FALSE && c2!=PULLUP && c2!=PULLMID && c2!=PULLDOWN) return false;
              if(c2==ANYBIT && c1!=TRUE && c1!=FALSE && c1!=PULLUP && c1!=PULLDOWN) return false;
              if(c2==ANYTRIT && c1!=TRUE && c1!=MAYBE && c1!=FALSE && c1!=PULLUP && c1!=PULLMID && c1!=PULLDOWN) return false;
           }
           else if((c1==TRUE || c1==PULLUP) && c2!=TRUE && c2!=PULLUP) return false;
           else if((c1==MAYBE || c1==PULLMID) && c2!=MAYBE && c2!=PULLMID) return false;
           else if((c1==FALSE || c1==PULLDOWN) && c2!=FALSE && c2!=PULLDOWN) return false;
           else if(c1!=c2) return false;
        }
        return true;
}

Wires Wires::min(const Wires &w) const
{
        int n = size();
        int m = w.size();
        if(m > n)
        {
           n = m;
           m = size();
        }
        Wires r(n,type());
        for(int i=0;i<n;i++)
        {
           if(i>=m)
           {
              r.set(i,INVALID);
              continue;
           }
           switch(get(i))
           {
              case TRUE:
              case PULLUP:
                switch(w.get(i))
                {
                   case TRUE:
                   case PULLUP:
                     r.set(i,TRUE);
                     break;
                   case MAYBE:
                   case PULLMID:
                     r.set(i,MAYBE);
                     break;
                   case FALSE:
                   case PULLDOWN:
                     r.set(i,FALSE);
                     break;
                   default:
                     r.set(i,INVALID);
                }
                break;
              case MAYBE:
              case PULLMID:
                switch(w.get(i))
                {
                   case TRUE:
                   case PULLUP:
                   case MAYBE:
                   case PULLMID:
                     r.set(i,MAYBE);
                     break;
                   case FALSE:
                   case PULLDOWN:
                     r.set(i,FALSE);
                     break;
                   default:
                     r.set(i,INVALID);
                }
                break;
              case FALSE:
              case PULLDOWN:
                switch(w.get(i))
                {
                   case TRUE:
                   case PULLUP:
                   case MAYBE:
                   case PULLMID:
                   case FALSE:
                   case PULLDOWN:
                     r.set(i,FALSE);
                     break;
                   default:
                     r.set(i,INVALID);
                }
                break;
              default:
                r.set(i,INVALID);
           }
        }
        return r;
}

Wires Wires::max(const Wires &w) const
{
        int n = size();
        int m = w.size();
        if(m > n)
        {
           n = m;
           m = size();
        }
        Wires r(n,type());
        for(int i=0;i<n;i++)
        {
           if(i>=m)
           {
              r.set(i,INVALID);
              continue;
           }
           switch(get(i))
           {
              case FALSE:
              case PULLDOWN:
                switch(w.get(i))
                {
                   case TRUE:
                   case PULLUP:
                     r.set(i,TRUE);
                     break;
                   case MAYBE:
                   case PULLMID:
                     r.set(i,MAYBE);
                     break;
                   case FALSE:
                   case PULLDOWN:
                     r.set(i,FALSE);
                     break;
                   default:
                     r.set(i,INVALID);
                }
                break;
              case MAYBE:
              case PULLMID:
                switch(w.get(i))
                {
                   case TRUE:
                   case PULLUP:
                     r.set(i,TRUE);
                     break;
                   case MAYBE:
                   case PULLMID:
                   case FALSE:
                   case PULLDOWN:
                     r.set(i,MAYBE);
                     break;
                   default:
                     r.set(i,INVALID);
                }
                break;
              case TRUE:
              case PULLUP:
                switch(w.get(i))
                {
                   case TRUE:
                   case PULLUP:
                   case MAYBE:
                   case PULLMID:
                   case FALSE:
                   case PULLDOWN:
                     r.set(i,TRUE);
                     break;
                   default:
                     r.set(i,INVALID);
                }
                break;
              default:
                r.set(i,INVALID);
           }
        }
        return r;
}

Wires Wires::inv() const
{
    int n = size();
    Wires r(n,type());
    for(int i=0;i<n;i++)
    {
       switch(get(i))
       {
          case FALSE:
          case PULLDOWN:
            r.set(i,TRUE);
            break;
          case MAYBE:
          case PULLMID:
            r.set(i,MAYBE);
            break;
          case TRUE:
          case PULLUP:
            r.set(i,FALSE);
            break;
          default:
            r.set(i,INVALID);
       }
    }
    return r;
}

/* Connection bodies */

int Connection::apply(char a)
{
#ifdef DEBUG_APPLY
  std::cout << "Apply " << a << " on " << c[1];
#endif
  if(a==INVALID || a==ANYBIT || a==ANYTRIT) c[1] = INVALID;
  else
   switch(c[1])
   {
    case TRUE:
      if(a==MAYBE || a==FALSE) c[1] = INVALID;
      break;
    case MAYBE:
      if(a==TRUE || a==FALSE) c[1] = INVALID;
      break;
    case FALSE:
      if(a==TRUE || a==MAYBE) c[1] = INVALID;
      break;
    case PULLUP:
    case PULLDOWN:
    case PULLMID:
      if(a==TRUE || a==MAYBE || a==FALSE) c[1] = a;
      break;
    case NC:
      c[1] = a;
      break;
/*
    case ANYBIT:
    case ANYTRIT:
    case INVALID:
*/
    default:
      c[1] = INVALID;
   }
#ifdef DEBUG_APPLY
  std::cout << " -> " << c[1] << std::endl;
#endif
  return c[1];
}

/* System bodies */

std::vector<Connection*> System::v;
std::map<std::string,int> System::m;
System* System::s = nullptr;

System* System::getInstance()
{
  if(s==nullptr)
     s = new System();
  return s;
}

int System::attach(Entity* e, const char* n, unsigned short k, char c)
{
  int oo,o=-1;
  if(k<1) return -1;
  for(int i=0;i<k;i++)
  {
     oo = -1;
     std::string name(n);
     if(*n=='.')
     {
        name = e->name() + name;
     }
     if(k>1)
     {
        name += "[";
        name += std::to_string(i);
        name += "]";
     }
     auto a = m.find(name);
     if(a==m.end())
     {
        v.push_back(new Connection(name.c_str()));
        oo = v.size()-1;
        m[name] = oo;
     }
     else oo = a->second;
     v[oo]->entity(e,c);
     if(o<0) o=oo;
#ifdef DEBUG_ATTACH
     std::cout << name << " <- " << e->name() << " (idx=" << oo << ")" << std::endl;
#endif
  }
  return o;
}

